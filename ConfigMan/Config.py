__author__ = 'eey'
import types
import json

from Option import Option, BooleanOption, StringOption, IntegerOption, FloatOption, load_option_from_dict


class Config(object):
    """
        Holds a set of configuration options.
    """

    def __init__(self, name, **kwargs):
        """
            Load the configuration, and validate some basics.

            :param name     The name for this configuration (debug, production, etc).
                            Should be at least 3 letters long.
        """
        from types import StringType

        # make sure that the configuration has been given a name
        if not isinstance(name, StringType) or len(name) < 3:
            error = "Configuration must be given a name (with a length of 3 or more letters)."
            raise ValueError(error)

        # print "Initializing new configuration '%s':\n" % name
        self.name = name

        # TODO read kwargs and add as options where possible?

        # for key, value in sorted(kwargs.iteritems()):
        #     print "\t%s: %s" % (str(key), str(value))
        #
        #     if not hasattr(self, key):
        #
        #         """
        #         # if we're adding a File/Directory Path, we'll add it as a property
        #         # this is so that we can later do 'f = open(config.filename)' instead of 'open(str(config.filename))'
        #         # or 'open(config.filename.path)'
        #
        #         if isinstance(value, Config.DirectoryPath) or isinstance(value, Config.FilePath):
        #             setattr(self, "_%s" % key, value)
        #
        #             def_property_getter = "def get_%s(self):\n" \
        #                                   "\treturn str(self._%s)" % (key, key)
        #
        #             set_property_getter = "%s = property(get_%s)" % (key, key)
        #
        #             print def_property_getter
        #             print set_property_getter
        #
        #             exec def_property_getter
        #             exec set_property_getter
        #
        #             print dir(self)
        #
        #         else:
        #             setattr(self, key, value)
        #         """
        #         setattr(self, key, value)
        #
        #     else:
        #         error = "Duplicated attribute '%s'" % str(key)
        #         self._print_error(error)
        #         raise Exception
        #
        # print '\nConfiguration loaded succesfully.\n'

    def add_option(self, name, option):
        """
            Add an option to Configuration.

            :param name:    Name for this parameter.
            :param option:  An instance of the Option class
            :return:        Nothing.
            :raises:        ValueError if an attribute with this name already exists.
        """

        # print type(Option)
        assert isinstance(option, Option), "Can only add instances of 'Option' class."

        # if this configuration already has an attribute with this name, raise an error
        if hasattr(self, name):
            raise ValueError("An attribute named '%s' already exists." % name)

        self.name = property(option.get_value, option.set_value, option.del_value, option.__doc__)

    def __setattr__(self, key, value):
        """
            Allows using dot notation to assign options to configuration.

            :param key:     The option's name.
            :param value:   An instance of 'Option'.
        """

        if type(value) in [type(Option), type(BooleanOption), type(StringOption), type(IntegerOption), type(FloatOption)]:
            self.add_option(key, value)

        else:
            # if we're not adding an instance of 'Option', use legacy function.
            if hasattr(self, key) and isinstance(getattr(self, key), Option):
                getattr(self, key).set_value(value)

            else:
                super(Config, self).__setattr__(key, value)

    def to_dict(self, dic=None, include_hidden=True):
        """
            Returns a dictionary containing a representation of this Configuration.

            :param dic:     If defined, keys will be added to this passed-in dictionary.
                            Intended to allow subclasses to only set the values for subclass-specific keys.
        """
        if dic is None:
            dic = {}
        elif not isinstance(dic, types.DictionaryType):
            raise TypeError("Optional parameter 'dic' must be a dictionary.")

        # TODO write Config's name to to the dictionary

        # iterate over all attributes and, if they're Option instances, call their to_dict() methods
        for attribute in dir(self):
            if isinstance(getattr(self, attribute), Option):
                # only include hidden attributes if include_hidden=True was passed
                if not getattr(self, attribute).hidden and include_hidden:      # TODO logic correct here?
                    dic[attribute] = getattr(self, attribute).to_dict()

        return dic

    def to_json(self, indent=None, include_hidden=True):
        """
            Convert configuration to a json string.

            :return: String containing JSON representation of all configuration options.
        """

        return json.dumps(self.to_dict(include_hidden=include_hidden), indent=indent)

    @classmethod
    def from_dict(cls, data):
        """
            Load a Configuration from a dictionary representation.

            :param data:    A dictionary containing a valid configuration.
            :return:        An instance of Config.
        """

        loaded_cfg = Config('loaded')   # TODO read from file
        for option_name in data:
            loaded_option = load_option_from_dict(data[option_name])
            loaded_cfg.__setattr__(option_name, loaded_option)

        return loaded_cfg

    @classmethod
    def from_json(cls, string):
        """
            Load a configuration from a JSON encoded string.

            :param string:
            :return:
        """

        data = json.loads(string)

        return cls.from_dict(data)

    def save(self, filename, indent=None):
        """
            Store a configuration as a json file.

            :param filename:    Path to file location.
            :return:
        """
        try:
            f = open(filename, 'w')

        except Exception, e:
            raise e

        try:
            json.dump(self.to_dict(), f, indent=indent)

        except Exception, e:
            raise e

        f.close()

    def __repr__(self):
        return str(self.to_json())

    def __unicode__(self):
        return unicode(self.to_json())



