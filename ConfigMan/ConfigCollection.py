__author__ = 'eey'
from Config import Config


class ConfigCollection(object):
    """
        A collection of configurations
    """

    def __init__(self):
        self.configs = {}

    def __getitem__(self, item):
        return self.configs[item]

    def add(self, new_config):
        """
            Add a configuration instance to the ConfigCollection

            :param new_config:      An instance of the Config class.
        """

        if not isinstance(new_config, Config):
            error = "Not an instance of Config: %s" % new_config
            raise ValueError(error)

        if new_config.name in self.configs:
            error = "A configuration named '%s' already exists."
            raise ValueError(error)

        else:
            self.configs[new_config.name] = new_config

    def load(self, filename):
        """
            TODO: Load a configuration from a file.
            TODO: implement this

            :param filename:
            :return:
        """
        pass

    def save(self, filename):
        """
            TODO: Write a configuration to a file.

            :param filename:
            :return:
        """
        pass

    def set_config(self, config_name):
        global config

        if config_name not in self.configs:
            print "Error: unknown configuration '%s'" % config_name
            raise Exception

        config = self.configs[config_name]

    @staticmethod
    def _print_error(error):
        print "Error while loading config: %s\n" % str(error)
