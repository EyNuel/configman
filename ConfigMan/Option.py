#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'eey'

import types
import json
import datetime


class Option(object):
    """
        A generic option. Intended to be subclassed to implement type-specific options.
        TODO convert 'Option' to an abstract base class

        Subclasses may implement their own get_value(), set_value(), del_value(), is_valid(), validate() as well
        as __repr__() and __unicode__()
    """

    type = None

    def __init__(self, label='', description='', is_required=True, default=None, hidden=False):
        """
            :param label:           A label to be shown in a user interface (optional).
            :param description:     A detailed description to be shown in a user interface (optional).
            :param is_required:     Whether a user provided value is mandatory (optional).
            :param default:         Default value for option (optional).
            :param hidden:          This is intended for hiding options from the user interface (optional).
                                    NOTE: this should be replaced by a full fledged permissions system.
        """
        self.label = label
        self.description = description
        self.is_required = is_required
        self.default = default
        self.hidden = hidden
        self._value = None

    def to_dict(self, dic=None, include_hidden=True):
        """
            Returns a dictionary containing a representation of this Option.

            :param dic:             If defined, keys will be added to this passed-in dictionary.
                                    Intended to allow subclasses to only set the values for subclass-specific keys,
                                    while the generic ones continue to be defined here.
            :param include_hidden:  Boolean indicating whether to include hidden fields in the output or not.
            :return:
        """
        if dic is None:
            dic = {}
        elif not isinstance(dic, types.DictionaryType):
            raise TypeError("Optional parameter 'dic' must be a dictionary.")

        dic['type'] = self.type
        dic['label'] = self.label
        dic['description'] = self.description
        dic['is_required'] = self.is_required
        dic['default'] = self.default
        dic['hidden'] = self.hidden
        dic['value'] = self.get_value()
        return dic

    def to_json(self, indent=None):
        return json.dumps(self.to_dict(), indent=indent)

    @classmethod
    def from_json(cls, string):
        """
            Load an option instance from its JSON representation.

            :param string:  A string containing valid JSON.
            :return:        An instance of Option
        """
        try:
            return cls.from_dict(json.loads(string))

        except Exception, e:
            raise e

    @classmethod
    def from_dict(cls, dic):
        """
            Create an option instance from a dictionary representation.

            :param  dic:    A dictionary representation of an Option.
            :return:        An instance of Option.
        """

        # NOTE: dict.get('key') returns 'None' if 'key' is not in dict. This allows us avoid using try-except blocks
        # data_type = dic.get('type')     # NOTE: this value must be set by the subclasse's 'from_dict' implementation
        label = dic.get('label')
        description = dic.get('description')
        is_required = dic.get('is_required')
        default = dic.get('default')
        hidden = dic.get('hidden')
        value = dic.get('value')

        option = cls(
            label=label,
            description=description,
            is_required=is_required,
            default=default,
            hidden=hidden
        )
        option.set_value(value)
        return option

    def get_value(self):
        if self._value:
            return self._value

        else:
            if self.default is None:
                if self.is_required:
                    raise ValueError('Option is required but has neither a value nor a default.')

                else:
                    return None

            else:
                return self.default

    def set_value(self, value):
        # TODO value's validity should be checked before assigning/storing
        self._value = value

    def del_value(self):
        pass

    def is_valid(self):
        """
            Return True if option attributes are valid.
        """
        if self.is_required and self._value is None:
            return False

        else:
            return True

    def validate(self):
        """
            Check this option's validity and raise an error if it isn't.

            :raises:    ValueError if not valid.
        """
        if not self.is_valid():
            raise ValueError

    def __repr__(self):
        """
            String representation of Option's value.
        """
        return str(self.get_value())

    def __unicode__(self):
        """
            Unicode representation of Option's value.
        """
        return unicode(self.get_value())

    def __bool__(self):
        """
            Boolean value of Option's value

            :returns:   For Booleans it returns the value's logical value;
                        for all other types it returns True if defined, False otherwise.
        """
        if self.get_value():
            return True

        else:
            return False

    __nonzero__ = __bool__


class BooleanOption(Option):
    """
        A type-specific option for booleans.
    """

    type = 'bool'

    def __init__(self, label='', description='', is_required=True, default=None, hidden=False):
        if default is not None and not isinstance(default, types.BooleanType):
            raise TypeError("Default value of a BooleanOption must be a Boolean.")
        else:
            super(BooleanOption, self).__init__(label=label, description=description, is_required=is_required, default=default, hidden=hidden)

    @classmethod
    def from_dict(cls, dic):
        """
            Instantiate a BooleanOption from a dictionary representation of a BooleanOption.

            :param dic: A dictionary to be loaded.
            :return:    An instance of
            :raises:    TypeError if not a BooleanOption.
        """

        # TODO the following code should be moved to the superclass to keep the code DRY
        try:
            if dic['type'] != BooleanOption.type:
                raise TypeError("Type is not 'bool': '%s'" % dic['type'])

        except KeyError:
            e = "A value for 'type' must be provided."
            raise KeyError(e)

        return super(BooleanOption, cls).from_dict(dic)


class IntegerOption(Option):
    """
        A type-specific option for Integer.
    """

    type = 'int'

    def __init__(self, label, description='', is_required=True, default=None, hidden=False, min_value=None, max_value=None):
        if default is not None and not isinstance(default, types.IntType):
            raise TypeError("Default value of an IntegerOption must be an Integer.")

        elif min_value is not None and not isinstance(min_value, types.IntType):
            raise TypeError("Min Value of an IntegerOption must be an Integer.")

        elif max_value is not None and not isinstance(max_value, types.IntType):
            raise TypeError("Min Value of an IntegerOption must be an Integer.")

        else:
            super(IntegerOption, self).__init__(label=label, description=description, is_required=is_required, default=default, hidden=hidden)
            self.min_value = min_value
            self.max_value = max_value

    def to_dict(self, dic=None):
        """
            Returns a dictionary containing a representation of this Option.

            :param dic:     If defined, keys will be added to this passed-in dictionary.
                            Intended to allow subclasses to only set the values for subclass-specific keys.
        """

        if dic is None:
            dic = {}
        elif not isinstance(dic, types.DictionaryType):
            raise TypeError("Optional parameter 'dic' must be a dictionary.")

        dic['min_value'] = self.min_value
        dic['max_value'] = self.max_value

        # call superclass' to_dict() to add the representation of the missing attributes to the dictionary
        return super(IntegerOption, self).to_dict(dic)

    @classmethod
    def from_dict(cls, dic):
        """
            Instantiate an IntegerOption from a dictionary representation of an IntegerOption.

            :param dic: A dictionary to be loaded.
            :return:    An instance of
            :raises:    TypeError if not an IntegerOption.
        """

        # TODO the following code should be moved to the superclass to keep the code DRY
        try:
            if dic['type'] != IntegerOption.type:
                raise TypeError("Type is not 'int': '%s'" % dic['type'])

        except KeyError:
            e = "A value for 'type' must be provided."
            raise KeyError(e)

        option = super(IntegerOption, cls).from_dict(dic)
        option.min_value = dic.get('min_value')
        option.max_value = dic.get('max_value')
        return option


class FloatOption(Option):
    """
        A type-specific option for Float.
    """

    type = 'float'

    def __init__(self, label, description='', is_required=True, default=None, hidden=False, min_value=None, max_value=None):
        if default is not None and not isinstance(default, types.FloatType):
            raise TypeError("Default value of an FloatOption must be a Float.")

        elif min_value is not None and not isinstance(min_value, types.FloatType):
            raise TypeError("Min Value of an FloatOption must be a Float.")

        elif max_value is not None and not isinstance(max_value, types.FloatType):
            raise TypeError("Min Value of an FloatOption must be a Float.")

        else:
            super(FloatOption, self).__init__(label=label, description=description, is_required=is_required, default=default, hidden=hidden)
            self.min_value = min_value
            self.max_value = max_value

    def to_dict(self, dic=None):
        """
            Returns a dictionary containing a representation of this Option.

            :param dic:     If defined, keys will be added to this passed-in dictionary.
                            Intended to allow subclasses to only set the values for subclass-specific keys.
        """

        if dic is None:
            dic = {}
        elif not isinstance(dic, types.DictionaryType):
            raise TypeError("Optional parameter 'dic' must be a dictionary.")

        dic['min_value'] = self.min_value
        dic['max_value'] = self.max_value

        # call superclass' to_dict() to add the representation of the missing attributes to the dictionary
        return super(FloatOption, self).to_dict(dic)

    @classmethod
    def from_dict(cls, dic):
        """
            Instantiate an FloatOption from a dictionary representation of an FloatOption.

            :param dic: A dictionary to be loaded.
            :return:    An instance of
            :raises:    TypeError if not an FloatOption.
        """

        # TODO the following code should be moved to the superclass to keep the code DRY
        try:
            if dic['type'] != FloatOption.type:
                raise TypeError("Type is not 'float': '%s'" % dic['type'])

        except KeyError:
            e = "A value for 'type' must be provided."
            raise KeyError(e)

        option = super(FloatOption, cls).from_dict(dic)
        option.min_value = dic.get('min_value')
        option.max_value = dic.get('max_value')
        return option


class StringOption(Option):
    """
        A type-specific option for String.
    """
    # TODO: add min_length and max_length parameters

    type = 'string'

    def __init__(self, label, description='', is_required=True, default=None, hidden=False):
        if default is not None and not isinstance(default, types.StringTypes):
            raise TypeError("Default value of an StringOption must be a String.")

        else:
            super(StringOption, self).__init__(label=label, description=description, is_required=is_required, default=default, hidden=hidden)

    @classmethod
    def from_dict(cls, dic):
        """
            Instantiate a StringOption from a dictionary representation of a StringOption.

            :param dic: A dictionary to be loaded.
            :return:    An instance of
            :raises:    TypeError if not a StringOption.
        """

        return super(StringOption, cls).from_dict(dic)


class DateOption(Option):
    """
        A type-specific option for Dates.
    """

    type = 'date'

    def __init__(self, label, description='', is_required=True, default=None, min_value=None, max_value=None, hidden=False):
        if default is not None and not isinstance(default, datetime.date):
            raise TypeError("Default value of a DateOption must be a 'datetime.date'.")

        elif min_value is not None and not isinstance(min_value, datetime.date):
            raise TypeError("Min Value of a DateOption must be a 'datetime.date'.")

        elif max_value is not None and not isinstance(max_value, datetime.date):
            raise TypeError("Min Value of a DateOption must be a 'datetime.date'.")

        else:
            super(FloatOption, self).__init__(label=label, description=description, is_required=is_required, default=default, hidden=hidden)
            self.min_value = min_value
            self.max_value = max_value

    def to_dict(self, dic=None):
        """
            Returns a dictionary containing a representation of this Option.

            :param dic:     If defined, keys will be added to this passed-in dictionary.
                            Intended to allow subclasses to only set the values for subclass-specific keys.
        """

        if dic is None:
            dic = {}
        elif not isinstance(dic, types.DictionaryType):
            raise TypeError("Optional parameter 'dic' must be a dictionary.")

        dic['min_value'] = self.min_value
        dic['max_value'] = self.max_value

        # call superclass' to_dict() to add the representation of the missing attributes to the dictionary
        return super(FloatOption, self).to_dict(dic)


class IPv4Option(Option):
    """
        Input should be either a string like '192.168.0.1' or a tuple like (192, 168, 0, 1)
        TODO
    """
    pass


class IPv6Option(Option):
    """
        TODO
    """
    pass


class Path(Option):
    """
        Common class from which DirectoryPath and FilePath are derived.

        TODO: create an __init__ function here and make use of it in subclasses.
    """
    def __str__(self):
        return self.path

    def __unicode__(self):
        return unicode(self.path)


class DirectoryPath(Path):
    """
        Contains a single directory path, stored as a string.
    """

    def __init__(self, path, is_required=True, default=None, create_if_inexistent=False):
        import os

        # Ensure that 'path' ends with a slash '/')
        if path[-1] is not '/':
            path += '/'

        # ensure that the directory exists:
        if not os.path.isdir(path):
            if create_if_inexistent:
                try:
                    os.makedirs(path)

                except:
                    error = "Failed to create directory '%s'" % str(path)
                    raise IOError(error)

            else:
                error = "Directory does not exist: '%s'" % str(path)
                raise IOError(error)

        self.path = path


class FilePath(Path):
    """
        Contains a single file path
    """

    def __init__(self, path, create_if_inexistent=False):
        import os

        # ensure that the file exists:
        if os.path.exists(path) and not os.path.isfile(path):
            error = "Not a file: '%s'" % str(path)
            raise IOError(error)

        elif os.path.exists(path) and os.path.isfile(path):
            # file exists, nothing to do here
            pass

        elif not os.path.exists(path):
            if create_if_inexistent:
                try:
                    open(path, 'a').close()

                except:
                    error = "Failed to create file '%s'" % str(path)
                    raise IOError(error)

            else:
                error = "File does not exist: '%s'" % str(path)
                raise IOError(error)

        else:
            error = "This should be impossible..."
            raise IOError(error)

        self.path = path


    """
        Load an Option from a json object.
def load_option_from_dict(option_dict):
    """
        Load a single Option from a json object.
        TODO move this into 'Option' definition as a staticmethod?!

        :return: Option instance containing the loaded json object.
    """

    data_types = ['bool', 'int', 'string', 'float']     # TODO generate this by iterating over the subclasses of 'Option' and reading their 'type' attributes. Also, call the correct cls.from_json() automatically

    # get datatype
    try:
        data_type = option_dict['type']

    except KeyError:
        e = "Invalid Option. No 'type' key found"
        raise KeyError(e)

    if data_type not in data_types:
        e = "Type '%s' is unknown. Type must be one of %s" % (data_type, data_types)
        raise ValueError(e)

    try:
        if data_type == 'bool':
            option = BooleanOption.from_dict(option_dict)

        elif data_type == 'string':
            option = StringOption.from_dict(option_dict)

        elif data_type == 'int':
            option = IntegerOption.from_dict(option_dict)

        elif data_type == 'float':
            option = FloatOption.from_dict(option_dict)

        else:
            raise NotImplementedError

    except NotImplementedError:
        raise NotImplementedError

    except Exception as e:
        raise e

    return option

