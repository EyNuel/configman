#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'eey'

#load default configuration from file
from default_config import default_config as config

print "Loaded Default Config:"
print config.to_json(2)

# override default config:
config.n_closest_locations = 15
config.max_mass_coeff = 0.7
config.avoid_tolls = True
config.hostname = 'production.server.org'

print 50*'='
print "Overridden Config:"
print config.to_json(2)

# TODO validate config
# default_config.validate()
