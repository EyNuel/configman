#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'eey'

from ConfigMan.Config import Config
from ConfigMan.Option import BooleanOption, IntegerOption, FloatOption, StringOption  # TODO DateTimeOption, DateOption

"""
General option parameters are:
    :param label:           A label to be shown in a user interface (optional).
    :param description:     A detailed description to be shown in a user interface (optional).
    :param default:         Default value for option (optional).
    :param hidden:          This is intended for hiding options from the user interface (optional).
                            NOTE: this should be replaced by a full fledged permissions system.

IntegerOption and FloatOption also allow:
    :param min_value:       Lowest valid value for option.
    :param max_value:       Highest valid value for option.
"""

# Set up the available config options
default_config = Config('opt')
default_config.n_closest_locations = IntegerOption(
    label="Número de locais de entrega vizinhos",
    min_value=0,
    max_value=100,
    default=15,
    description="Quando um novo local de entrega é inserido na base de dados, são automáticamente calculados os " \
                "caminhos ida/volta entre este novo local e os seus vizinhos mais proximos."
)
default_config.max_mass_coeff = FloatOption(
    label="Coeficiente de carga máxima dos veiculos",
    min_value=0.0,
    max_value=1.0,
    default=0.7
)
default_config.avoid_tolls = BooleanOption(
    label='Evitar Autoestradas',
    default=False,
)
default_config.hostname = StringOption(
    label='Host Name',
    default='debug.server.org'
)
# default_config.erp_date_zero = DateOption(
#     # todo
# )
# default_config.some_relevant_instant_in_time = DateTimeOption(
#     # todo
# )
