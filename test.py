#!/usr/bin/env python
# -*- coding: utf-8 -*-

# TODO convert this to an actual set of unit tests!

# from ConfigMan.ConfigCollection import ConfigCollection
from ConfigMan.Config import Config
from ConfigMan.Option import Option, BooleanOption, FilePath, DirectoryPath
import datetime


# Set up the configuration object
cfg = Config(name='default')
cfg.debug = Option(label='Enable debugging output', is_required=True, default=False)
cfg.verbose = Option(label='Make output more verbose', is_required=True, default=True)
cfg.db_host = Option(label='', is_required=True, default='localhost')
cfg.web_host = Option(label='', is_required=False, default=None)


## Lets verify that the different ways of accessing the options have the same value:
print "1) A Generic option containing a boolean value which was not defined and which has a default value of 'False'."
print type(cfg.debug)
print cfg.debug
print cfg.debug.get_value()
print cfg.debug._value
if cfg.debug:
    print True
else:
    print False
print 55*'=' + '\n'

print "2) A Generic option containing a boolean value which was not defined and which has a default value of 'True'."
print type(cfg.verbose)
print cfg.verbose
print cfg.verbose.get_value()
print cfg.verbose._value
if cfg.verbose:
    print True
else:
    print False
print 55*'=' + '\n'

print "3) A Generic option containing a string with a default value."
print type(cfg.db_host)
print cfg.db_host
print cfg.db_host.get_value()
print cfg.db_host._value
if cfg.db_host:
    print True
else:
    print False
print 55*'=' + '\n'

print "4) A Generic option containing a string without a default value."
print type(cfg.web_host)
print cfg.web_host
print cfg.web_host.get_value()
print cfg.web_host._value
if cfg.web_host:
    print True
else:
    print False

print 2*(55*'='+'\n')

## Replace with type-specific options and repeat tests
print "5) A BooleanOption containing a value which was not defined and which has a default value of False."
cfg.debug_1 = BooleanOption(label='', is_required=True, default=False)
print type(cfg.debug_1)
print cfg.debug_1
print cfg.debug_1.get_value()
print cfg.debug_1._value
if cfg.debug_1:
    print True
else:
    print False
print 55*'=' + '\n'

print "6) A BooleanOption containing a value which was not defined and which has a default value of True."
cfg.verbose_1 = BooleanOption(label='', is_required=True, default=True)
print type(cfg.verbose_1)
print cfg.verbose_1
print cfg.verbose_1.get_value()
print cfg.verbose_1._value
if cfg.verbose_1:
    print True
else:
    print False
print 55*'=' + '\n'

# print "7) A BooleanOption containing a value which was not defined and which has a default value of 'string'. This should produce an error."
# cfg.error = BooleanOption(is_required=True, default='string')
# print type(cfg.error)
# print cfg.error
# print cfg.error.get_value()
# print cfg.error._value
# if cfg.error:
#     print True
# else:
#     print False
# print 55*'=' + '\n'


print "8) Lets manually set a BooleanOption to False"
cfg.debug_1 = False
print type(cfg.debug_1)
print cfg.debug_1
print cfg.debug_1.get_value()
print cfg.debug_1._value
if cfg.debug_1:
    print True
else:
    print False
print 55*'=' + '\n'

print "9) Get a dictionary representation of a type-agnostic option"
print cfg.debug.to_dict()
print 55*'=' + '\n'

print "10) Get a json representation of a type-agnostic option"
print cfg.debug.to_json(indent=2)
print 55*'=' + '\n'

print "11) Get a dictionary representation of a boolean option"
print cfg.debug_1.to_dict()
print 55*'=' + '\n'

print "12) Get a json representation of a boolean option"
print cfg.debug_1.to_json(indent=2)
print 55*'=' + '\n'

print "13) Get a json representation of an IntegerOption"
# print cfg.debug_1.to_json(indent=2)
# print 55*'=' + '\n'
# TODO

print "14) Get a dictionary representation of an IntegerOption"
# print cfg.to_dict()
# print 55*'=' + '\n'
# TODO

print "15) Get a dictionary representation of a Config"
print cfg.to_dict()
print 55*'=' + '\n'

print "16) Get a json representation of a Config"
print cfg.to_json(indent=2)
print 55*'=' + '\n'

print "16) write a json file contaiing the config"
cfg.save('config_test.json', indent=2)


