#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'eey'

import json
from ConfigMan.Config import Config
from ConfigMan.Option import load_option_from_dict, BooleanOption, StringOption, IntegerOption, FloatOption

# Set up the configuration object
cfg = Config(name='default')
cfg.debug = BooleanOption(label='Enable debugging output', is_required=True, default=False)

# TODO convert this to unit tests

print cfg.to_dict()
print 20*'-'


print 'Verify that we can load a BooleanOption from a dictionary:'
d = cfg.debug.to_dict()
print d
print 20*'-'
option_from_dict = BooleanOption.from_dict(d)
print option_from_dict.to_json(indent=2)
print 20*'='
print

print 'Verify that we can load a BooleanOption from JSON:'
j = cfg.debug.to_json(indent=2)
print j
print 20*'-'
option_from_json = BooleanOption.from_json(j)
print option_from_json.to_json(indent=2)
print 20*'='
print

print 'Verify that we can load a BooleanOption from a dict without assuming knowledge about its type:'
print d
print 20*'-'
option_from_dict_2 = load_option_from_dict(d)
print option_from_dict_2.to_json(indent=2)
print 20*'='
print

print 'Verify that we can load a StringOption from a dict without assuming knowledge about its type:'
cfg.host_name = StringOption(label='Server Host Name')
cfg.host_name = 'www.somewhere.net'
d_string = cfg.host_name.to_dict()
print "d_string: %s" % d_string
print 20*'.'
option_from_dict_3 = load_option_from_dict(d_string)
print option_from_dict_3.to_json(indent=2)
print 20*'='
print

print 'Verify that we can load an IntegerOption from a dict without assuming knowledge about its type:'
cfg.n_locs = IntegerOption(label='Number of Locations', min_value=0, max_value=30, default=15)
cfg.n_locs = 5
d_int = cfg.n_locs.to_dict()
print "d_float: %s" % d_int
print 20*'.'
option_from_dict_4 = load_option_from_dict(d_int)
print option_from_dict_4.to_json(indent=2)
print

print 'Verify that we can load a FloatOption from a dict without assuming knowledge about its type:'
cfg.max_mass_coeff = FloatOption(label='Max Mass Coefficient', min_value=0.0, max_value=1.0, default=0.66)
cfg.max_mass_coeff = 0.7
d_float = cfg.max_mass_coeff.to_dict()
print "d_float: %s" % d_float
print 20*'.'
option_from_dict_5 = load_option_from_dict(d_float)
print option_from_dict_5.to_json(indent=2)
print

# write to file:
cfg.save('file_read_test.json', indent=2)

# load from file
f = open('file_read_test.json', 'r')

print 'Verify that we can read a complete configuration from a dictionary'
data_string = f.read()
decoded = json.loads(data_string)
cfg_loaded_1 = Config.from_dict(decoded)

print 45*'='
print "written to file:"
print cfg.to_json(indent=2)
print
print 45*'='
print 'loaded from file:'
print cfg_loaded_1.to_json(indent=2)
print


print 'Verify that we can read a complete configuration from a JSON string'
cfg_loaded_2 = Config.from_json(data_string)

print 45*'='
print "written to file:"
print cfg.to_json(indent=2)
print
print 45*'='
print 'loaded from file:'
print cfg_loaded_2.to_json(indent=2)

